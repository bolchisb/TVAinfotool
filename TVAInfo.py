import json
import requests
import sys
import os
from PyQt5 import QtWidgets, uic


class PopMessage(QtWidgets.QMessageBox):
    def __init__(self):
        QtWidgets.QMessageBox.__init__(self)
        self.move(QtWidgets.QApplication.desktop().screen().rect().center() - self.rect().center())

    def warning_mess(self, text1, text2):
        QtWidgets.QMessageBox.warning(self, text1, text2, QtWidgets.QMessageBox.Yes)

    def info_mess(self, text1, text2):
        QtWidgets.QMessageBox.information(self, text1, text2, QtWidgets.QMessageBox.Yes)

    def critical_mess(self, text1, text2):
        QtWidgets.QMessageBox.critical(self, text1, text2, QtWidgets.QMessageBox.Yes)


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self, parent)
        file_path = os.path.abspath('main.ui')
        uic.loadUi(file_path, self)
        self.move(QtWidgets.QApplication.desktop().screen().rect().center() - self.rect().center())
        self.searchBtn.pressed.connect(self.searchFunction)


    def searchFunction(self):
        line = self.input_line.displayText()
        if line.isnumeric():
            data1 = {'cui': self.input_line.displayText(), 'data': '2018-04-27'}
            r = requests.post("https://webservicesp.anaf.ro/PlatitorTvaRest/api/v3/ws/tva", json=[data1])
            data = r.json()
            self.lineEdit_2.setText(str(data['found'][0]['cui']))
            self.lineEdit_3.setText(str(data['found'][0]['data']))
            self.lineEdit_4.setText(data['found'][0]['denumire'])
            self.textBrowser.setText(data['found'][0]['adresa'])
            self.lineEdit_6.setText(str(data['found'][0]['scpTVA']))
            self.lineEdit_7.setText(data['found'][0]['data_inceput_ScpTVA'])
            self.lineEdit_8.setText(data['found'][0]['data_sfarsit_ScpTVA'])
            self.lineEdit.setText(data['found'][0]['data_anul_imp_ScpTVA'])
            self.lineEdit_9.setText(data['found'][0]['mesaj_ScpTVA'])
            self.lineEdit_10.setText(data['found'][0]['dataInceputTvaInc'])
            self.lineEdit_11.setText(data['found'][0]['dataSfarsitTvaInc'])
            self.lineEdit_12.setText(data['found'][0]['dataActualizareTvaInc'])
            self.lineEdit_13.setText(data['found'][0]['dataPublicareTvaInc'])
            self.lineEdit_14.setText(data['found'][0]['tipActTvaInc'])
            self.lineEdit_15.setText(str(data['found'][0]['statusTvaIncasare']))
            self.lineEdit_17.setText(data['found'][0]['dataInactivare'])
            self.lineEdit_18.setText(data['found'][0]['dataReactivare'])
            self.lineEdit_19.setText(data['found'][0]['dataPublicare'])
            self.lineEdit_20.setText(data['found'][0]['dataRadiere'])
            self.lineEdit_5.setText(str(data['found'][0]['statusInactivi']))
            self.lineEdit_21.setText(str(data['found'][0]['dataInceputSplitTVA']))
            self.lineEdit_22.setText(str(data['found'][0]['dataAnulareSplitTVA']))
            self.lineEdit_23.setText(str(data['found'][0]['statusSplitTVA']))
        else:
            PopMessage().critical_mess('TVA Error', 'Va rugam sa introduceti Codul Unic de Inregistrare fara litere.')


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    main_window = MainWindow()
    main_window.show()
    sys.exit(app.exec_())